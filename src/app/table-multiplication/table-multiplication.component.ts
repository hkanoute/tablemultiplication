import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss'],
})
export class TableMultiplicationComponent implements OnInit {
  ngOnInit(): void {}

  @Input() nombre!: number;
  tableau = [1,2,3,4,5,6,7,8,9,10];
  
}
