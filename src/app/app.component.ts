import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  tableForm!: FormGroup;
  emptyForm = false;
  isSubmitted = false;
  nombre = 0;
  nombreDeTable = 10;
  maximumTable = false

  constructor() {}

  ngOnInit(): void {
    this.tableForm = new FormGroup({
      nombre: new FormControl(),
      nombreDeTable : new FormControl()
    });
  }
  numberSubmitted() : void {
    this.isSubmitted = true;
    if (this.tableForm.value.nombre == null) {
      this.emptyForm = true;
    }
    else {
      this.nombre = this.tableForm.value.nombre;
      if(this.tableForm.value.nombreDeTable != null && (this.tableForm.value.nombreDeTable <=10 && this.tableForm.value.nombreDeTable > 0 )){
        this.nombreDeTable = this.tableForm.value.nombreDeTable;
      }else{
        this.maximumTable = true;
      }
      this.emptyForm = false;
      this.maximumTable = false;
      console.log(this.nombre,this.nombreDeTable)
    }
  }
}
