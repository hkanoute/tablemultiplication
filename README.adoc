= TableMultiplication
Kanoute Hamidou SIO22
Dépôt GitLab : https://gitlab.com/hkanoute/tablemultiplication.git

= Objectif

Ce projet est divisé en deux partie,

=== Première partie :

Conception d'une application web qui affiche une table de multiplication allant de 1 à 10 selon une valeur soumise par l'utilisateur.

=== Deuxième partie :

Affichage des tables de multiplication autour de la table de multiplication saisie par l'utilisateur, l'utilisateur aura le choix de choisir le nombre de tables de multiplication à afficher qui est limité et par défaut à 10.

= Presentation des composants

Cette application est composé de trois composants :

image::src/assets/uml.png[]

= Première partie

[,js]
-----
export class AppComponent {


  tableForm!: FormGroup;
  emptyForm = false;
  isSubmitted = false;
  nombre = 0;
  nombreDeTable = 10;
  maximumTable = false

  constructor() {}

  ngOnInit(): void {
    this.tableForm = new FormGroup({
      nombre: new FormControl(),
      nombreDeTable : new FormControl()
    });
  }
  numberSubmitted() : void {
    this.isSubmitted = true;
    if (this.tableForm.value.nombre == null) {
      this.emptyForm = true;
    }
    else {
      this.nombre = this.tableForm.value.nombre;
      if(this.tableForm.value.nombreDeTable != null && this.tableForm.value.nombreDeTable <=10){
        this.nombreDeTable = this.tableForm.value.nombreDeTable;
      }else{
        this.maximumTable = true;
      }
      this.emptyForm = false;
      this.maximumTable = false;
      console.log(this.nombre,this.nombreDeTable)
    }
  }
}
-----
La classe AppComponent s'occupe de la gestion du formulaire ainsi que de l'envoi des attributs nombre et nombreDeTable ainsi que des vérifications de saisies grâce à la méthode `numberSubmitted()`

[,HTML]
----

<section>
    <div class="container">
        <div class="column is-centered">
            <h3 class="title is-3">table de multiplication</h3>
            <h4>veuillez saisir un nombre</h4>
            <form [formGroup]='tableForm' (ngSubmit)='numberSubmitted()'>
                <p [ngClass]="{ 'has-error': isSubmitted && emptyForm || maximumTable }">
                 <input class="input is-succes" formControlName="nombre" type="number" placeholder="nombre à multiplier" value="">
                 <input class="input is-succes" formControlName="nombreDeTable" type="number" placeholder="Nombre de table afficher à par défaut et limité à 10 " max="10">
                </p>
                <div *ngIf="isSubmitted && emptyForm" class="help-block">
                    <div *ngIf="emptyForm">Un nombre est demandé !</div>
                </div>
                 <div class="field is-grouped">
                    <div class="control" >
                      <button class="button">Soumettre</button>
                    </div>
                 </div>
            </form>
        </div>

    </div>

</section>
<div *ngIf="isSubmitted && (!emptyForm)">
    <app-table-multiplication [nombre]="nombre" >
    </app-table-multiplication>
</div>
----
Dans le composant `app.component.html` nous avons créé un formulaire nommé `tableForm` avec deux input `nombre` et `nombreDeTable`. De plus les deux input sont définis comme étant de type `number` ce qui fait que l'utilisateur ne peut entrer que des nombres, il ne reste plus qu'à vérifier que l'utilisateur a saisi un nombre avant d'accepter l'envoi du formulaire grâce à la méthode `numberSubmitted()`. Lorsque l'utilisateur a bien rempli le premier input le composant `app.component` envoi au composant fille `table-multiplication` le nombre saisi par l'utilisateur.
[,js]
----
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss'],
})
export class TableMultiplicationComponent implements OnInit {
  ngOnInit(): void {}

  @Input() nombre!: number;
  tableau = [1,2,3,4,5,6,7,8,9,10];

}
----
Nous récupérons la valeur de l'input "nombre" grâce au décorateur `@input()` dans le composant `table-multiplication` et déclarons un tableau contenant des entiers allants de 1 à 10.

[,html]
----
<style>
  table, th , td  {
    border: 1px solid grey;
    border-collapse: collapse;
    padding: 5px;
    margin: auto;
  }
  table tr:nth-child(odd) {
    background-color: #f1f1f1;
  }
  table tr:nth-child(even) {
    background-color: #ffffff;
  }
  </style>
<div >
  <table class="has-text-centered">
    <tr *ngFor="let i of tableau">
      <th>{{ nombre }}x{{ i }} = {{ nombre * i }}</th>
    </tr>
  </table>
</div>
----
Dans la vue du composant `table.multiplication` nous avons créé un tableau dans lequel nous avons une boucle parcourant l'attribut `tableau` et pour chaque élément présent dans l'attribut tableau nous réalisons la consigne dans les balises th, ce qui va nous créé notre tableau de multiplication.Il ne reste plus qu'a appelé le composant dans app.component.html avec les balises `<app-table-multiplication [nombre]="nombre" ></app-table-multiplication>`
ce qui nous donne ce résultat :

image::src/assets/tableau.png[]

= Deuxième partie

[,js]
----
import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-tables-multiplication',
  templateUrl: './tables-multiplication.component.html',
  styleUrls: ['./tables-multiplication.component.scss']
})
export class TablesMultiplicationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() nombreDeTable! : number;
  @Input() nombre!: number;
  tableau : number[] = [1,2,3,4,5,6,7,8,9,10];

}

----
Dans cette deuxièeme partie nous avons le composant `tables-multiplication` qui va s'occuper de créer plusieurs table de multiplication. Il reçoit d' `app.component` les deux valeurs saisie par l'utilisateur dans le formulaire `tableForm`.

[, html]
-----
<style>
  table, th , td  {
    border: 1px solid grey;
    border-collapse: collapse;
    padding: 5px;
    margin:auto
  }
  table tr:nth-child(odd) {
    background-color: #f1f1f1;

  }
  table tr:nth-child(even) {
    background-color: #ffffff;

  }
  </style>
  <p class="has-text-centered" *ngIf="nombre !null">Le(s) {{nombreDeTable}} tables de multiplication avant celui de {{nombre}}</p>
<table *ngIf="nombre !null" >
  <tr *ngFor="let i of tableau"  >
    <th *ngIf="nombreDeTable >= 0">{{ nombre-tableau[0] }}x{{ i }} = {{ (nombre-tableau[0]) * i }}</th>
    <th *ngIf="nombreDeTable >= 2">{{ nombre-tableau[1] }}x{{ i }} = {{ (nombre-tableau[1]) * i }}</th>
    <th *ngIf="nombreDeTable >= 3">{{ nombre-tableau[2] }}x{{ i }} = {{ (nombre-tableau[2]) * i }}</th>
    <th *ngIf="nombreDeTable >= 4">{{ nombre-tableau[3] }}x{{ i }} = {{ (nombre-tableau[3]) * i }}</th>
    <th *ngIf="nombreDeTable >= 5">{{ nombre-tableau[4] }}x{{ i }} = {{ (nombre-tableau[4]) * i }}</th>
    <th *ngIf="nombreDeTable >= 6">{{ nombre-tableau[5] }}x{{ i }} = {{ (nombre-tableau[5]) * i }}</th>
    <th *ngIf="nombreDeTable >= 7">{{ nombre-tableau[6] }}x{{ i }} = {{ (nombre-tableau[6]) * i }}</th>
    <th *ngIf="nombreDeTable >= 8">{{ nombre-tableau[7] }}x{{ i }} = {{ (nombre-tableau[7]) * i }}</th>
    <th *ngIf="nombreDeTable >= 9">{{ nombre-tableau[8] }}x{{ i }} = {{ (nombre-tableau[8]) * i }}</th>
    <th *ngIf="nombreDeTable >= 10">{{ nombre-tableau[9] }}x{{ i }} = {{ (nombre-tableau[9]) * i }}</th>
  </tr>
</table>
-----

Dans la vue du composant `tables-multiplication` nous avons créé un tableau avec dix collone, la valeur de `nombreDeTable` est par défaut à 10 et change si l'utilisateur entre une valeur dans le deuxième input du formulaire, sa valeur ne change que si la valeur saisie est entre 1 et 10 (vérification effectué dans la méthode `numberSubmitted` du composant `app.component`).Ensuite nous affichons  les x  tables de multiplication
avant le nombre saisie par l'utilisateur.

ce qui nous donne :

image::src/assets/tableau2.png[]

= Diffucltés rencontrées

Dans la deuxième partie, nous pouvons voir une redondance au niveau de l'affichage, avant cette version, j'avais ce code :
[,html]
----
<style>
table, th , td  {
border: 1px solid grey;
border-collapse: collapse;
padding: 5px;
margin:auto
}
table tr:nth-child(odd) {
background-color: #f1f1f1;

    }
    table tr:nth-child(even) {
      background-color: #ffffff;

    }
    </style>
  <table class="has-text-centered">
    <tr *ngFor="let i of tableau">
      <th *ngFor="let j of tableau">{{ tableau[j-1] }}x{{ i }} = {{ tableau[j-1] * i }}</th>
    </tr>
  </table>
----

Ce code donne comme la version utilisée plus haut les dix tables de multiplication, je n'ai pas pu modifier les tables et donc les adaptée à la table saisie par l'utilisateur.Ainsi je suis resté sur la version non optimisé.








